package com.example.hsleiden_ikpmd.studieapp.activity;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.database.DatabaseHelper;
import com.example.hsleiden_ikpmd.studieapp.fragment.CourseChoiceFragment;
import com.example.hsleiden_ikpmd.studieapp.fragment.DashboardFragment;
import com.example.hsleiden_ikpmd.studieapp.fragment.OverviewFragment;
import com.example.hsleiden_ikpmd.studieapp.gson.CourseRequest;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;
import com.example.hsleiden_ikpmd.studieapp.network.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Boolean fragmentLoad = false;
    private DashboardFragment dashboardFragment;
    private OverviewFragment overviewFragment;
    private CourseChoiceFragment courseChoiceFragment;
    private Toolbar toolbar;
    private List<CourseModel> courseModels = new ArrayList<>();
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Add toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        // Menu
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        databaseHelper = DatabaseHelper.getInstance(this);

        setCourseList();

        if( ! courseModels.isEmpty()){
            Log.d("Cached", "Load from cash");
            createFragments();
        }

        requestCourses();
    }

    private void setCourseList() {
        courseModels = databaseHelper.getCourseList();
    }

    private void createFragments() {
        Log.d("Fragment", "Create");

        // Create fragments
        dashboardFragment = DashboardFragment.newInstance();
        overviewFragment = OverviewFragment.newInstance();
        courseChoiceFragment = CourseChoiceFragment.newInstance();

        this.fragmentLoad = true;
        // Open dashboard
        goTo(dashboardFragment, false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_dashboard)
            goTo(dashboardFragment);
        if (id == R.id.nav_overview)
            goTo(overviewFragment);
        if (id == R.id.nav_courseChoice)
            goTo(courseChoiceFragment);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goTo(Fragment fragment)
    {
        goTo(fragment, true);
    }
    public void goTo(Fragment fragment, boolean addToStack)
    {
        // Set title
        if(fragment == dashboardFragment)
            toolbar.setTitle("Dashboard");
        if(fragment == overviewFragment)
            toolbar.setTitle("Overzicht");
        if(fragment == courseChoiceFragment)
            toolbar.setTitle("Keuzevakken");

        // Set replacement
        if(addToStack)
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).addToBackStack( "A1" ).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).commit();
    }

    private void requestCourses() {
        String server_url = "http://crefick.nl/ikpmd/courses.php";
        CourseRequest<List<CourseModel>> CourseRequest = new CourseRequest<>(server_url, new Response.Listener<List<CourseModel>>() {
            @Override
            public void onResponse(List<CourseModel> response) {
                databaseHelper.processResponse(response);
                processCourseList(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Do something
                error.printStackTrace();
            }
        });
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(CourseRequest);
    }

    private void processCourseList(List<CourseModel> response) {
        for( CourseModel course : response)
        {
            courseModels.add(course);
        }

        if( ! this.fragmentLoad)
        {
            createFragments();
        }
    }
}
