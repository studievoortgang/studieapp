package com.example.hsleiden_ikpmd.studieapp.gson;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Bart van der Laan on 1/22/2017.
 */

public class CourseRequest<T> extends Request<T> {

    private final Gson gson = new Gson();
    private final Response.Listener<T> listener;

    public CourseRequest(String url, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = listener;
    }

    @Override
    protected void deliverResponse (T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        Type type = new TypeToken<List<CourseModel>>(){}.getType();

        try {
            String json = new String(response.data);
            return (Response<T>) Response.success(gson.fromJson(json, type), HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e){
            return Response.error(new ParseError(e));
        }

    }
}
