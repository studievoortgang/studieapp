package com.example.hsleiden_ikpmd.studieapp.model;

/**
 * Created by Bart van der Laan on 1/13/2017.
 */

public class CourseModel {

    private String name;
    private String code;
    private String description;
    private int ec;
    private int period;
    private boolean freechoice;

    public CourseModel(String name, String code, String description, int ec, int period) {
        this.name = name;
        this.code = code;
        this.description = description;
        this.ec = ec;
        this.period = period;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public int getEC() {
        return ec;
    }

    public int getPeriod() {
        return period;
    }

    public boolean getFreeChoice() {
        return freechoice;
    }
}