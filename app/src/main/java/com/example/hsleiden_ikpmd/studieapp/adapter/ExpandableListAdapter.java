package com.example.hsleiden_ikpmd.studieapp.adapter;

/**
 * Created by bvanhoekelen on 16-01-17.
 */

import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;
import com.example.hsleiden_ikpmd.studieapp.model.MarkModel;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Activity context;
    private Map<String, List<String>> lessonCollection;
    private List<String> laptops;
    private List<MarkModel> markModels;

    public ExpandableListAdapter(Activity context, List<String> laptops, Map<String, List<String>> laptopCollections, List<MarkModel> markModels) {
        this.context = context;
        this.lessonCollection = laptopCollections;
        this.laptops = laptops;
        this.markModels = markModels;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return lessonCollection.get(laptops.get(groupPosition)).get(childPosition);
    }

     public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
     }

    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String course_code = (String) getChild(groupPosition, childPosition);

        LayoutInflater inflater = context.getLayoutInflater();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_item, null);
        }

        TextView item = (TextView) convertView.findViewById(R.id.child_code);
        TextView mark = (TextView) convertView.findViewById(R.id.child_mark);

        item.setText(course_code);
        mark.setText("geen");

        for (MarkModel markModel : markModels)
        {
            if( course_code.equals(markModel.getCode()) && ! markModel.getMark().equals(""))
            {
                mark.setText(markModel.getMark() + "");
                break;
            }
        }


        return convertView;
    }

    public int getChildrenCount(int groupPosition) {return lessonCollection.get(laptops.get(groupPosition)).size();}

    public Object getGroup(int groupPosition) {return laptops.get(groupPosition);}

    public int getGroupCount() {return laptops.size();}

    public long getGroupId(int groupPosition) { return groupPosition;}

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String laptopName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_item,null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.group_period);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(laptopName);

        return convertView;
    }

    public boolean hasStableIds() {return true;}
    public boolean isChildSelectable(int groupPosition, int childPosition) {return true;}
}