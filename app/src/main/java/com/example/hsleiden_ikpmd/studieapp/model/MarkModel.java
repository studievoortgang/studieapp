package com.example.hsleiden_ikpmd.studieapp.model;

/**
 * Created by Bart van der Laan on 1/13/2017.
 */

public class MarkModel {

    private String code;
    private String mark;

    public MarkModel(String code, String mark) {
        this.code = code;
        this.mark = mark;
    }

    public String getCode() {
        return code;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public void setCode(String code) {
        this.code = code;
    }
}