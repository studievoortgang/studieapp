package com.example.hsleiden_ikpmd.studieapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.activity.AddMarkActivity;
import com.example.hsleiden_ikpmd.studieapp.adapter.ExpandableListAdapter;
import com.example.hsleiden_ikpmd.studieapp.database.DatabaseHelper;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;
import com.example.hsleiden_ikpmd.studieapp.model.MarkModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.data;

/**
 * Created by bvanhoekelen on 19-01-17.
 */

public class OverviewFragment extends Fragment implements View.OnClickListener {

    List<String> groupList;
    List<String> childList;
    Map<String, List<String>> lessonCollection;
    ExpandableListView expListView;
    private List<CourseModel> courseModels = new ArrayList<>();
    private List<MarkModel> markModels = new ArrayList<>();
    private DatabaseHelper databaseHelper;

    public OverviewFragment() {
    }

    public static OverviewFragment newInstance() {
        OverviewFragment fragment = new OverviewFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.overview_fragment, container, false);
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
        databaseHelper = DatabaseHelper.getInstance(getContext());
        courseModels = databaseHelper.getCourseList();
        markModels = databaseHelper.getMarkList();

        createGroupList();
        createCollection();

        expListView = (ExpandableListView) getView().findViewById(R.id.laptop_list);
        final ExpandableListAdapter expListAdapter = new ExpandableListAdapter(getActivity(), groupList, lessonCollection, markModels);
        expListView.setAdapter(expListAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
                final String selected = (String) expListAdapter.getChild(groupPosition, childPosition);
                Intent addMarkIntent = new Intent(getActivity(), AddMarkActivity.class);
                addMarkIntent.putExtra("course_code", selected);
                startActivity(addMarkIntent);
                return true;
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    private void createGroupList() {
        groupList = new ArrayList<String>();
        groupList.add("Periode 1");
        groupList.add("Periode 2");
        groupList.add("Periode 3");
        groupList.add("Periode 4");
    }

    private void createCollection() {
        lessonCollection = new LinkedHashMap<String, List<String>>();
        for (String laptop : groupList) {
            if (laptop.equals("Periode 1"))
                loadCourseChild(1);
            if (laptop.equals("Periode 2"))
                loadCourseChild(2);
            if(laptop.equals("Periode 3"))
                loadCourseChild(3);
            if(laptop.equals("Periode 4"))
                loadCourseChild(4);

            lessonCollection.put(laptop, childList);
        }
    }

    private void loadCourseChild(int period) {
        childList = new ArrayList<String>();
        for (CourseModel course : courseModels)
        {
            if(course.getPeriod() == period /*&& course.getFreeChoice() == false*/)
                childList.add(course.getCode());
        }
    }
}
