package com.example.hsleiden_ikpmd.studieapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.model.ChoiceModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Bart van der Laan on 1/28/2017.
 */

public class ChoiceCourseExpListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> mListHeaderData;
    private HashMap<String, List<String>> mListItemData;
    private List<ChoiceModel> choiceModels;

    public ChoiceCourseExpListAdapter(Context context, List<String> listHeaderData, HashMap<String, List<String>> listItemData, List<ChoiceModel> choiceModels) {
        this.mContext = context;
        this.mListHeaderData = listHeaderData;
        this.mListItemData = listItemData;
        this.choiceModels = choiceModels;
    }

    @Override
    public int getGroupCount() {
        return this.mListHeaderData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mListItemData.get(this.mListHeaderData.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListHeaderData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mListItemData.get(this.mListHeaderData.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.explv_group_ccourse, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.cCourseListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.explv_item_ccourse, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.cCourseListItem);
        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
        txtListChild.setText(childText);

        for (ChoiceModel choiceModel : choiceModels)
        {
            if (choiceModel.getCode().matches(childText) && choiceModel.getChoosen()) {
                checkBox.setChecked(true);
                break;
            } else {
                checkBox.setChecked(false);
            }
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setNewItems(List<String> listDataHeader,HashMap<String, List<String>> listChildData, List<ChoiceModel> choiceModels) {
        this.mListHeaderData = listDataHeader;
        this.mListItemData = listChildData;
        this.choiceModels = choiceModels;
        notifyDataSetChanged();
    }

}


