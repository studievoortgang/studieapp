package com.example.hsleiden_ikpmd.studieapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.database.DatabaseHelper;
import com.example.hsleiden_ikpmd.studieapp.model.MarkModel;

import static java.security.AccessController.getContext;

public class AddMarkActivity extends AppCompatActivity implements View.OnClickListener {

    private String course_code;
    private Button btn;
    private EditText mark;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        course_code = intent.getStringExtra("course_code");
        setContentView(R.layout.activity_add_mark);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_activity_add_mark) + " " + course_code);
        setSupportActionBar(toolbar);

        // Set items
        databaseHelper = DatabaseHelper.getInstance(this);
        btn = (Button) findViewById(R.id.btn_add_mark);
        mark = (EditText) findViewById(R.id.etxt_mark);
        btn.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {

        MarkModel markModel = new MarkModel(course_code, mark.getText() + "");
        databaseHelper.setMark(markModel);

        Toast.makeText(this, "Het cijfer " + markModel.getMark() + " is aan " + markModel.getCode() + " toegevoegd", Toast.LENGTH_LONG).show();

        finish();
    }
}
