package com.example.hsleiden_ikpmd.studieapp.model;

/**
 * Created by Bart van der Laan on 1/13/2017.
 */

public class ChoiceModel {

    private String code;
    private Boolean choosen;

    public ChoiceModel(String code, Boolean choosen) {
        this.code = code;
        this.choosen = choosen;
    }

    public String getCode() {
        return code;
    }

    public Boolean getChoosen() {
        return choosen;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public void setChoosen(Boolean choosen) {
        this.choosen = choosen;
    }
}