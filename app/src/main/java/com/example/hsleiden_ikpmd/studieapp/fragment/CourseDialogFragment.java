package com.example.hsleiden_ikpmd.studieapp.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.Property;

import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;

/**
 * Created by Bart van der Laan on 27-1-2017.
 */

public class CourseDialogFragment extends DialogFragment {

    private CourseModel courseModel;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)  {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String message = "Geen vak gevonden!";
        if (courseModel != null) {
            String nl = System.getProperty("line.separator");
            message = courseModel.getName() + nl+nl + courseModel.getEC() + " EC" + nl+nl + courseModel.getDescription();
        }

        builder.setMessage(message)
                .setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    public void setCourseModel(CourseModel courseModel) {
        this.courseModel = courseModel;
    }
}
