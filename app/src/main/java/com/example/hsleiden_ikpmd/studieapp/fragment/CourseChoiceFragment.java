package com.example.hsleiden_ikpmd.studieapp.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.adapter.ChoiceCourseExpListAdapter;
import com.example.hsleiden_ikpmd.studieapp.database.DatabaseHelper;
import com.example.hsleiden_ikpmd.studieapp.model.ChoiceModel;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CourseChoiceFragment extends Fragment {

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private List<CourseModel> courseModels;
    private final DatabaseHelper dbHelper = DatabaseHelper.getInstance(getContext());
    private List<ChoiceModel> choiceModels;

    public CourseChoiceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CourseChoiceFragment.
     */
    public static CourseChoiceFragment newInstance() {
        CourseChoiceFragment fragment = new CourseChoiceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_course_choice, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ExpandableListView explv_courses = (ExpandableListView) view.findViewById(R.id.explv_courses);

        prepareListData();

        choiceModels = dbHelper.getChoices();
        final ChoiceCourseExpListAdapter listAdapter = new ChoiceCourseExpListAdapter(getContext(), listDataHeader, listDataChild, choiceModels);
        explv_courses.setAdapter(listAdapter);

        explv_courses.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
                final String selected = (String) listAdapter.getChild(groupPosition, childPosition);
                boolean follow = true;

                for (ChoiceModel choiceModel : choiceModels) {
                    if (choiceModel.getCode().matches(selected) && choiceModel.getChoosen()) {
                        follow = false;
                        break;
                    }
                }
                dbHelper.setChoice(selected, follow);
                choiceModels = dbHelper.getChoices();
                listAdapter.setNewItems(listDataHeader, listDataChild, choiceModels);
                return true;
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void prepareListData() {

        Log.d("Method: ", "prepareListData");

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        courseModels = dbHelper.getChoiceCourseList();

        //Adding Headers
        listDataHeader.add("Periode 1");
        listDataHeader.add("Periode 2");
        listDataHeader.add("Periode 3");
        listDataHeader.add("Periode 4");

        //Adding Child data
        List<String> p1List = new ArrayList<String>();
        List<String> p2List = new ArrayList<String>();
        List<String> p3List = new ArrayList<String>();
        List<String> p4List = new ArrayList<String>();

        for (CourseModel course : courseModels) {
            switch (course.getPeriod()) {
                case 1:
                    p1List.add(course.getCode());
                    break;
                case 2:
                    p2List.add(course.getCode());
                    break;
                case 3:
                    p3List.add(course.getCode());
                    break;
                case 4:
                    p4List.add(course.getCode());
                    break;
            }
        }

        listDataChild.put(listDataHeader.get(0), p1List);
        listDataChild.put(listDataHeader.get(1), p2List);
        listDataChild.put(listDataHeader.get(2), p3List);
        listDataChild.put(listDataHeader.get(3), p4List);
    }
}
