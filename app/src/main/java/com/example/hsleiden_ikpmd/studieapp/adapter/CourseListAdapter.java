package com.example.hsleiden_ikpmd.studieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;

import java.util.List;

/**
 * Created by Bart van der Laan on 1/13/2017.
 */

public class CourseListAdapter extends ArrayAdapter<CourseModel> {

    public CourseListAdapter(Context context, int resource, List<CourseModel> courses) {
        super(context, resource, courses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CourseModel course = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.courselist_content_row, parent, false);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.course_name);
        TextView tvCode = (TextView) convertView.findViewById(R.id.course_code);

        tvName.setText(course.getName());
        tvCode.setText(course.getCode());

        return convertView;
    }
}
