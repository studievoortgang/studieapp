package com.example.hsleiden_ikpmd.studieapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.hsleiden_ikpmd.studieapp.model.ChoiceModel;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;
import com.example.hsleiden_ikpmd.studieapp.model.MarkModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static android.R.id.list;

/**
 * Created by Bart van der Laan on 1/22/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static SQLiteDatabase mSQLDB;
    private static DatabaseHelper mInstance;
    public static final String dbName = "studieapp.db";
    public static final int dbVersion = 4;

    public DatabaseHelper(Context context) {
        super(context, dbName, null, dbVersion);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(context);
            mSQLDB = mInstance.getWritableDatabase();
        }

        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseInfo.CourseTable.COURSE + " (" +
                DatabaseInfo.CourseColumn.NAME + " TEXT, " +
                DatabaseInfo.CourseColumn.CODE + " TEXT PRIMARY KEY, " +
                DatabaseInfo.CourseColumn.DESCRIPTION + " TEXT, " +
                DatabaseInfo.CourseColumn.EC + " INTEGER, " +
                DatabaseInfo.CourseColumn.PERIOD + " INTEGER);"
        );
        db.execSQL("CREATE TABLE " + DatabaseInfo.ChoiceTable.CHOICE + " (" +
                DatabaseInfo.ChoiceColumn.CODE + " TEXT PRIMARY KEY, " +
                DatabaseInfo.ChoiceColumn.CHOOSEN + " INTEGER ); "
        );
        db.execSQL("CREATE TABLE " + DatabaseInfo.MarkTable.MARK + " (" +
                DatabaseInfo.CourseColumn.CODE + " TEXT PRIMARY KEY, " +
                DatabaseInfo.CourseColumn.MARK + " TEXT ); "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.CourseTable.COURSE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.ChoiceTable.CHOICE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.MarkTable.MARK);
        onCreate(db);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void insert(String table, String nullColumnHack, ContentValues values) {
        mSQLDB.insertWithOnConflict(table, nullColumnHack, values, 5);
    }

    public void update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        mSQLDB.update(table, values, whereClause, whereArgs);
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectArgs, String groupBy, String having, String orderBy) {
        return mSQLDB.query(table, columns, selection, selectArgs, groupBy, having, orderBy);
    }

    public List<CourseModel> getCourseList(){
        //Retrieve Courses from SQLiteDB
        List<CourseModel> list = new ArrayList<CourseModel>();
        //Cursor resultSet = query(DatabaseInfo.CourseTable.COURSE, new String[] {"*"}, null, null, null, null, null);

        String query = "SELECT " +
                DatabaseInfo.CourseColumn.NAME + ", " +
                DatabaseInfo.CourseColumn.CODE + ", " +
                DatabaseInfo.CourseColumn.DESCRIPTION + ", " +
                DatabaseInfo.CourseColumn.EC + ", " +
                DatabaseInfo.CourseColumn.PERIOD +
                " FROM " + DatabaseInfo.CourseTable.COURSE +
                " LEFT OUTER JOIN " + DatabaseInfo.ChoiceTable.CHOICE +
                " ON " + DatabaseInfo.CourseColumn.CODE + " = " + DatabaseInfo.ChoiceColumn.CODE +
                " WHERE " + DatabaseInfo.ChoiceColumn.CHOOSEN + " = 1" +
                " OR " + DatabaseInfo.ChoiceColumn.CHOOSEN + " IS NULL";

        Cursor resultSet = mSQLDB.rawQuery(query, null, null);

        try {
            while (resultSet.moveToNext()) {
                String name = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.NAME));
                String code = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.CODE));
                String description = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.DESCRIPTION));
                int ec = resultSet.getInt(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.EC));
                int period = resultSet.getInt(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.PERIOD));
                //boolean freeChoice = ( resultSet.getInt(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.FREECHOICE)) != 0);

                CourseModel course = new CourseModel(name, code, description, ec, period);
                list.add(course);
            }
        } finally {
            resultSet.close();
        }

        return  list;
    }

    public List<CourseModel> getChoiceCourseList() {
        List<CourseModel> list = new ArrayList<>();

        Log.d("Method: ", "getChoiceCourseList");

        String query = "SELECT " +
                DatabaseInfo.CourseColumn.NAME + ", " +
                DatabaseInfo.CourseColumn.CODE + ", " +
                DatabaseInfo.CourseColumn.DESCRIPTION + ", " +
                DatabaseInfo.CourseColumn.EC + ", " +
                DatabaseInfo.CourseColumn.PERIOD +
                " FROM " + DatabaseInfo.CourseTable.COURSE +
                " JOIN " + DatabaseInfo.ChoiceTable.CHOICE +
                " ON " + DatabaseInfo.CourseColumn.CODE + " = " + DatabaseInfo.ChoiceColumn.CODE;

        Cursor resultSet = mSQLDB.rawQuery(query, null, null);

        Log.d("resultset", "Cursor: " + resultSet.getCount());

        try {
            while (resultSet.moveToNext()) {
                String name = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.NAME));
                String code = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.CODE));
                String description = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.DESCRIPTION));
                int ec = resultSet.getInt(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.EC));
                int period = resultSet.getInt(resultSet.getColumnIndex(DatabaseInfo.CourseColumn.PERIOD));

                CourseModel course = new CourseModel(name, code, description, ec, period);
                list.add(course);
            }
        } finally {
             resultSet.close();
        }

        return list;
    }

    public void processResponse(List<CourseModel> response) {
        for (CourseModel course : response) {
            ContentValues values = new ContentValues();
            values.put(DatabaseInfo.CourseColumn.NAME, course.getName());
            values.put(DatabaseInfo.CourseColumn.CODE, course.getCode());
            values.put(DatabaseInfo.CourseColumn.DESCRIPTION, course.getDescription());
            values.put(DatabaseInfo.CourseColumn.EC, course.getEC());
            values.put(DatabaseInfo.CourseColumn.PERIOD, course.getPeriod());
            this.insert(DatabaseInfo.CourseTable.COURSE, null, values);

            if (course.getFreeChoice() == true) {
                ContentValues chValues = new ContentValues();
                chValues.put(DatabaseInfo.ChoiceColumn.CODE, course.getCode());
                chValues.put(DatabaseInfo.ChoiceColumn.CHOOSEN, 0);
                this.insert(DatabaseInfo.ChoiceTable.CHOICE, null, chValues);
            }
        }
    }

    public List<MarkModel> getMarkList(){

        //Retrieve Courses from SQLiteDB
        List<MarkModel> list = new ArrayList<>();
        Cursor resultSet = query(DatabaseInfo.MarkTable.MARK, new String[] {"*"}, null, null, null, null, null);

        try {
            while (resultSet.moveToNext()) {
                String mark = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.MarkColumn.MARK));
                String code = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.MarkColumn.CODE));
                MarkModel markModel = new MarkModel(code, mark);
                list.add(markModel);
            }
        } finally {
            resultSet.close();
        }

        return list;
    }

    public void setMark(MarkModel mark)
    {
        ContentValues values = new ContentValues();
        values.put(DatabaseInfo.MarkColumn.CODE, mark.getCode());
        values.put(DatabaseInfo.MarkColumn.MARK, mark.getMark());
        this.insert(DatabaseInfo.MarkTable.MARK, null, values);
    }

    public List<ChoiceModel> getChoices() {
        List<ChoiceModel> list = new ArrayList<>();
        Cursor resultSet= query(DatabaseInfo.ChoiceTable.CHOICE, new String[] {"*"}, null, null, null, null, null);

        try {
            while (resultSet.moveToNext()) {
                String code = resultSet.getString(resultSet.getColumnIndex(DatabaseInfo.ChoiceColumn.CODE));
                boolean chosen = ( resultSet.getInt(resultSet.getColumnIndex(DatabaseInfo.ChoiceColumn.CHOOSEN)) != 0);
                ChoiceModel choiceModel = new ChoiceModel(code, chosen);
                list.add(choiceModel);
            }
        } finally {
            resultSet.close();
        }

        return list;
    }

    public void setChoice(String code, boolean follow) {
        ContentValues values = new ContentValues();
        String where = DatabaseInfo.ChoiceColumn.CODE + "=" + "'" + code + "'";
        values.put(DatabaseInfo.ChoiceColumn.CHOOSEN, follow);
        this.update(DatabaseInfo.ChoiceTable.CHOICE, values, where, null);
    }
}
