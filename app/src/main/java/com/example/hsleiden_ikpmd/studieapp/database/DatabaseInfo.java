package com.example.hsleiden_ikpmd.studieapp.database;

/**
 * Created by Bart van der Laan on 1/22/2017.
 */

public class DatabaseInfo {

    public class CourseTable {
        public static final String COURSE = "course";
    }

    public class CourseColumn {
        public static final String NAME = "name";
        public static final String CODE = "code";
        public static final String DESCRIPTION = "description";
        public static final String EC = "ec";
        public static final String PERIOD = "period";
        public static final String MARK = "mark";
        public static final String FREECHOICE = "freechoice";
    }

    public class MarkTable {
        public static final String MARK = "mark";
    }

    public class MarkColumn {
        public static final String CODE = "code";
        public static final String MARK = "mark";
    }

    public class ChoiceTable {
        public static final String CHOICE = "choice";
    }

    public class ChoiceColumn {
        public static final String CODE = "ccode";
        public static final String CHOOSEN = "choosen";
    }
}


