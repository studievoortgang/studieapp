package com.example.hsleiden_ikpmd.studieapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.hsleiden_ikpmd.studieapp.R;
import com.example.hsleiden_ikpmd.studieapp.adapter.CourseListAdapter;
import com.example.hsleiden_ikpmd.studieapp.database.DatabaseHelper;
import com.example.hsleiden_ikpmd.studieapp.model.CourseModel;
import com.example.hsleiden_ikpmd.studieapp.model.MarkModel;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by bvanhoekelen on 19-01-17.
 */

public class DashboardFragment extends Fragment implements View.OnClickListener  {

    private List<CourseModel> courseModels = new ArrayList<>();
    private List<MarkModel> markModels = new ArrayList<>();
    private DatabaseHelper databaseHelper;
    private int currentPeriod;
    private FragmentActivity mContext;
    private int progressStatus = 0;
    private ProgressBar bar;
    private Handler handler = new Handler();

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dashboard_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        databaseHelper = DatabaseHelper.getInstance(getContext());
        courseModels = databaseHelper.getCourseList();
        markModels = databaseHelper.getMarkList();

        int p1 = 0;
        int p2 = 0;
        int p3 = 0;
        int p4 = 0;

        for (CourseModel courseModel : courseModels) {
            for (MarkModel markModel : markModels) {
                double mark;
                try {
                    mark = Double.parseDouble(markModel.getMark());
                } catch (NumberFormatException e) {
                    mark = 0;
                }
                if (markModel.getCode().matches(courseModel.getCode()) && Double.compare(mark, 5.5) == 1) {
                    if (courseModel.getPeriod() == 1) {
                        p1 = courseModel.getEC() + p1;
                    } else if (courseModel.getPeriod() == 2) {
                        p2 = courseModel.getEC() + p2;
                    } else if (courseModel.getPeriod() == 3) {
                        p3 = courseModel.getEC() + p3;
                    } else {
                        p4 = courseModel.getEC() + p4;
                    }
                }
            }
        }
        GraphView graph = (GraphView) view.findViewById(R.id.ecGraph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 0),
                new DataPoint(1, p1),
                new DataPoint(2, p2),
                new DataPoint(3, p3),
                new DataPoint(4, p4)
        });
        graph.addSeries(series);

        progressStatus = p1+p2+p3+p4;
        bar = (ProgressBar) view.findViewById(R.id.progressBar);
        bar.setMax(60);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressStatus < 60) {
                    handler.post(new Runnable(){
                        public void run(){
                            bar.setProgress(progressStatus);
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        List<CourseModel> shortList = new ArrayList<>();

        for( CourseModel course : courseModels )
        {
            if(course.getPeriod() == 1)
                shortList.add(course);
        }

        ListView courseList = (ListView) view.findViewById(R.id.courseList);

        courseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                CourseDialogFragment myDialog = new CourseDialogFragment();
                myDialog.setCourseModel((CourseModel) adapterView.getItemAtPosition(position));
                myDialog.show(mContext.getFragmentManager(), "ChooseCourseDialog");
            }
        });

        CourseListAdapter adapter = new CourseListAdapter(getContext(), 0, shortList);
        courseList.setAdapter(adapter);
    }

    //Unused at the moment
    private void setCurrentPeriod() {
        String period1 = "05/09";
        String period2 = "21/11";
        String period3 = "13/02";
        String period4 = "08/05";

        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm");
        try {
            Date p1Date = sdf.parse(period1);
            Date p2Date = sdf.parse(period2);
            Date p3Date = sdf.parse(period3);
            Date p4Date = sdf.parse(period4);

            Date cDate = new Date();

            if (cDate.after(p1Date) && cDate.before(p2Date)) {
                currentPeriod = 1;
            } else if (cDate.after(p2Date) && cDate.before(p3Date)) {
                currentPeriod = 2;
            } else if (cDate.after(p3Date) && cDate.before(p4Date)) {
                currentPeriod = 3;
            } else if (cDate.after(p4Date)){
                currentPeriod = 4;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

    }
}
